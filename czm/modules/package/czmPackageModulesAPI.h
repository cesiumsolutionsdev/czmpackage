/* -*-c++-*- Saccades - Copyright (C) 2009-2022 Cesium Solutions
 * This file is subject to the terms and conditions defined in the file
 * 'SACCADES-LICENSE.txt', which is part of this source code distribution.
 */
#ifndef czm_modules_package_czmPackageModulesAPI_h
#define czm_modules_package_czmPackageModulesAPI_h

#ifdef __cplusplus
extern "C" {
#endif

/** @file czmPackageModulesAPI.h
 *  @defgroup package_modules Modules : Package
 *  @brief Definitions for Package Module API
 */
/* Definitions for exporting or importing the Saccades Package Module API */
#if defined( _MSC_VER ) || defined( __CYGWIN__ ) || defined( __MINGW32__ ) ||  \
    defined( __BCPLUSPLUS__ ) || defined( __MWERKS__ )
#  if defined czmpackagemodules_STATIC
#    define CZMPACKAGEMODULES_API
#  elif defined czmpackagemodules_EXPORTS
#    define CZMPACKAGEMODULES_API __declspec( dllexport )
#  else
#    define CZMPACKAGEMODULES_API __declspec( dllimport )
#  endif
#else
#  define CZMPACKAGEMODULES_API
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* czm_modules_package_czmPackageModulesAPI_h */
