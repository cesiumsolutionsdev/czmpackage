/* -*-c++-*- Saccades - Copyright (C) 2009-2022 Cesium Solutions
 * This file is subject to the terms and conditions defined in the file
 * 'SACCADES-LICENSE.txt', which is part of this source code distribution.
 */
#include "TemplateModule.hpp"

#include <igs/database/ModuleDevSource.hpp>

// ----------------------------------------------------------------------------
// pkg::Template Module definitions
// ----------------------------------------------------------------------------

// clang-format off
DEFINE_ENUMINFO(
    pkg::Template::Mode,
    "Template Mode", false, 3,
    pkg::Template::Mode_Default, "Default", "Default mode",
    pkg::Template::Mode_1,       "Mode_1",  "Mode 1",
    pkg::Template::Mode_2,       "Mode_2",  "Mode 2"
);
// clang-format off

namespace igs {

template <>
ClassInfo const &
classInfo<pkg::Template>()
{
  // clang-format off
  static ClassInfo info = ClassInfo().init<pkg::Template>()

      .addConstructor(
          constructor< pkg::Template, boost::shared_ptr<pkg::Template> >()
      )

      .addProperty(
          Property( "Mode", Property::Persistent,
              Method( "getMode", &pkg::Template::getMode,
                  Return( "mode", pkg::Template::Mode_Default, Return::Out )
              ),
              Method( "setMode", &pkg::Template::setMode,
                  Parameter( "mode", pkg::Template::Mode_Default, Parameter::In )
              ),
              makeSignal( "modeChanged", &pkg::Template::modeChanged,
                  Parameter( "mode", pkg::Template::Mode_Default, Parameter::Out )
              )
          )
      )

  ;
  // clang-format on

  return info;
} // classInfo<pkg::Template>

} // namespace igs

DEFINE_MODULE_CLASS( pkg::Template, pkg, Template );
