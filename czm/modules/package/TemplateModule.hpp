/* -*-c++-*- Saccades - Copyright (C) 2009-2022 Cesium Solutions
 * This file is subject to the terms and conditions defined in the file
 * 'SACCADES-LICENSE.txt', which is part of this source code distribution.
 */
#ifndef czm_modules_package_TemplateModule_hpp
#define czm_modules_package_TemplateModule_hpp

#include <czm/modules/package/czmPackageModulesAPI.h>

#include <czm/package/Template.hpp>

#include <igs/database/ModuleDevHeader.hpp>

DECLARE_MODULE_CLASS( pkg::Template, pkg, Template, CZMPACKAGEMODULES_API, );
TYPETRAITS( pkg::Template, true, false, false, true );

DECLARE_ENUMINFO( pkg::Template::Mode,
                  pkg,
                  TemplateMode,
                  CZMPACKAGEMODULES_API );

#endif // czm_modules_package_TemplateModule_hpp
