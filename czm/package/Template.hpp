/* -*-c++-*- Saccades - Copyright (C) 2009-2022 Cesium Solutions
 * This file is subject to the terms and conditions defined in the file
 * 'SACCADES-LICENSE.txt', which is part of this source code distribution.
 */
#ifndef czm_package_Template_hpp
#define czm_package_Template_hpp

#include <czm/package/czmPackageAPI.h>

#include <igs/boost/signal_version.hpp>

namespace pkg {

/**
 * @brief Provide a brief description of the class.
 * @tparam T Template parameter description (if present)
 * @ingroup package
 * @see Reference something else
 * @note Provide a note
 *
 * Provide a detailed description of the class.
 *
 * With possibly some example code:
 * <code>
 * Template template;
 * template.modeChanged.connect(
 *     []( Mode mode ) {
 *       std::cout << "Mode changed to: " << mode << '\n';
 *     }
 * );
 * <endcode>
 */
class CZMPACKAGE_API Template
{
public:
  /** @{ */
  /**
   * @name Structors
   */
  Template();
  ~Template();
  /** @} */

  /** @{ **/
  /**
   * @brief Methods to access and modify the Mode.
   * @param mode The @Mode to set.
   * @return The current mode.
   *
   * More detailed description.
   */

  enum Mode {
    Mode_Default, /**< The default mode. */
    Mode_1,       /**< The first mode. */
    Mode_2        /**< The second mode. */
  };

  Mode getMode() const
  {
    return mMode;
  }
  void                            setMode( Mode mode );
  BOOST_SIGNAL<void( Mode mode )> modeChanged;

  /** @} */

private:
  Mode mMode; /**< Post documentation of members. */

}; // class Template

} // namespace pkg

#endif // czm_package_Template_hpp
