/* -*-c++-*- Saccades - Copyright (C) 2009-2022 Cesium Solutions
 * This file is subject to the terms and conditions defined in the file
 * 'SACCADES-LICENSE.txt', which is part of this source code distribution.
 */
#ifndef czm_package_czmPackageAPI_h
#define czm_package_czmPackageAPI_h

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup Package : Package
 * @namespace pkg
 * @brief Namespace for all Package-related classes.
 */

/**
 * @file czmPackageAPI.h
 * @brief Definitions for Package Library API
 *
 */

/* Definitions for exporting or importing the Saccades Package Library API */
#if defined( _MSC_VER ) || defined( __CYGWIN__ ) || defined( __MINGW32__ ) ||  \
    defined( __BCPLUSPLUS__ ) || defined( __MWERKS__ )
#  if defined czmpackage_STATIC
#    define CZMPACKAGE_API
#  elif defined czmpackage_EXPORTS
#    define CZMPACKAGE_API __declspec( dllexport )
#  else
#    define CZMPACKAGE_API __declspec( dllimport )
#  endif
#else
#  define CZMPACKAGE_API
#endif

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* czm_package_czmPackageAPI_h */
