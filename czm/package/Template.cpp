/* -*-c++-*- Saccades - Copyright (C) 2009-2022 Cesium Solutions
 * This file is subject to the terms and conditions defined in the file
 * 'SACCADES-LICENSE.txt', which is part of this source code distribution.
 */

// Header corresponding to this source is always listed first with the exception
// of precompiled header.
#include "Template.hpp"

// Group headers from high-level (most dependent) to low level (least dependent)
// separating groups with a blank line as clang-format will automatically
// alphabetize based on groups of headers divided by a blank line

// Include header files local to this package
#include "SomeOtherClass.hpp"

// Include OpenIGS/Saccades headers
#include <igs/osg/scenegraph/Switch.hpp>
#include <igs/osg/scenegraph/Transform.hpp>

#include <igs/qt/gui/Dialog.hpp>
#include <igs/qt/gui/PushButton.hpp>

#include <igs/math/Quaternion.hpp>
#include <igs/math/Vector.hpp>

#include <igs/util/Log.hpp>
#include <igs/util/StringUtils.hpp>

// Include high level extern library headers

#include <osg/Camera>
#include <osg/Node>

#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>

// Include low level external library headers

#include <boost/any.hpp>
#include <boost/date_time.hpp>

// Include C++ system headers
#include <iostream>
#include <vector>

// Include C system headers
#include <cstdio>
#include <cstring>

namespace pkg {

// ----------------------------------------------------------------------------
// Template Section
// ----------------------------------------------------------------------------

Template::Template() : mMode( Mode_Default )
{
} // Template::Template

Template::~Template()
{
} // Template::~Template

void
Template::setMode( Mode mode )
{
  if ( mode == mMode ) {
    return;
  }
  mMode = mode;
  modeChanged( mMode );
} // Template::setMode

} // namespace pkg
