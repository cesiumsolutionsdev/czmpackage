/* -*-c++-*- Saccades - Copyright (C) 2009-2022 Cesium Solutions
 * This file is subject to the terms and conditions defined in the file
 * 'SACCADES-LICENSE.txt', which is part of this source code package.
 */

#include <czm/package/Template.hpp>

#include <iostream>

int
main( int argc, char ** argv )
{
  std::cout << "Running: " << argv[0] << '\n';

  pkg::Template tmpl;
  tmpl.setMode( pkg::Template::Mode_1 );
}
