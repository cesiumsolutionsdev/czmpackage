list( APPEND CMAKE_MODULE_PATH
    ${CMAKE_CURRENT_SOURCE_DIR}/share/saccades/cmake
)
##  include( SaccadesSetupExtern )

add_subdirectory( czm )
add_subdirectory( apps )
add_subdirectory( share )

