# --------------------------
# Set up Extern dependency
# --------------------------

if ( WIN32 )
  if ( NOT EXTERN_VERSION )
    set( EXTERN_VERSION 0.0.0 CACHE STRING "Extern Version" )
  endif()

  if ( NOT EXTERN_DIR )
    if ( SACCADES_EXTERN_DIR )
      set( EXTERN_DIR ${SACCADES_EXTERN_DIR}/Extern-${EXTERN_VERSION}
           CACHE PATH "Extern root directory" PARENT )
    elseif ( NOT WIN32 )
      set( EXTERN_DIR /usr
           CACHE PATH "Extern root directory" PARENT )
    else()
      message( FATAL_ERROR "Neither EXTERN_DIR nor SACCADES_EXTERN_DIR are set, set one of these appropriately." )
      return()
    endif()
  endif()

  find_package( Extern ${EXTERN_VERSION} REQUIRED )

  set( IGS_RUNTIME_DIRS ${IGS_RUNTIME_DIRS} ${EXTERN_DIR}/bin CACHE INTERNAL "" )

else()

  find_package( Extern REQUIRED COMPONENTS )

endif()

include_directories( ${EXTERN_INCLUDE_DIR} )

