# Specify identifiers
set( NAME TEXTURES )
set( name textures )
set( PARENT EUROSAT )

# Specify files
set(
    ${NAME}_FILES
    ${name}/alu1.png
    ${name}/alu1.rgb
    ${name}/alu1.rgb.attr
    ${name}/BlackFoil.rgb
    ${name}/BlackFoil.rgb.attr
    ${name}/BlackFoil2.rgb
    ${name}/BlackFoil2.rgb.attr
    ${name}/body-main.png
    ${name}/body-main.svg
    ${name}/cesium-banner2-512.rgb
    ${name}/diffuse.rgb
    ${name}/diffuse.rgb.attr
    ${name}/EuroSimForSimSolutionsLogo.rgb
    ${name}/GoldFoil.png
    ${name}/GoldFoil.rgb
    ${name}/GoldFoil.rgb.attr
    ${name}/'mirror tiles.rgb'
    ${name}/'mirror tiles.rgb.attr'
    ${name}/SIMsolarpanel.rgb
    ${name}/SIMsolarpanel.rgb.attr
    ${name}/solarpanal3.png
    ${name}/solarpanal3.rgb
    ${name}/solarpanal3.rgb.attr
    ${name}/solarpanalback.rgb
    ${name}/solarpanalback.rgb.attr
    ${name}/textures.cmake
    ${name}/uvCheckers.png
)

# -----------------------------------------------------------------------------
# The following are automated based on the above values.
# Should be no need to change them if you follow the naming conventions.
# -----------------------------------------------------------------------------
set(
    ${NAME}_CMAKE
    ${name}/${name}.cmake
)
source_group(
    \\${name} FILES
    ${${NAME}_FILES}
    ${${NAME}_CMAKE}
)
list(
    APPEND ${PARENT}_FILES
    ${${NAME}_FILES}
    ${${NAME}_CMAKE}
)
file(
    RELATIVE_PATH
    INSTALL_DIR
    ${PROJECT_SOURCE_DIR}
    ${CMAKE_CURRENT_LIST_DIR}
)
install(
    FILES ${${NAME}_FILES}
    DESTINATION ${INSTALL_DIR}
)
