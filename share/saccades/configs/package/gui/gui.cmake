set( SUBDIR gui )
set( SUBNAME GUI )
set( ${SUBNAME}_FILES
    ${SUBDIR}/composite.igsml
    ${SUBDIR}/composite.ui
    ${SUBDIR}/container.igsml
    ${SUBDIR}/dock.igsml
    ${SUBDIR}/gui.igsml
    ${SUBDIR}/settings.igsml
    ${SUBDIR}/tab.igsml
)

# ---------------------------------------------------------
set(
    ${SUBNAME}_CMAKE
    ${SUBDIR}/${SUBDIR}.cmake
)
source_group(
    \\${SUBDIR} FILES
    ${GUI_FILES}
    ${GUI_CMAKE}
)
set(
    ${SUBNAME}_ALL_FILES
    ${${SUBNAME}_FILES}
    ${${SUBNAME}_CMAKE}
)
install(
    FILES ${${SUBNAME}_FILES}
    DESTINATION share/saccades/configs/package/${SUBDIR}
)
