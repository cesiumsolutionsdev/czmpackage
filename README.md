This is the development source for Cesium Solution's Package Modules.
---------------------------------------------------------------------

This directory contains a skeleton setup for a Module package using
Cesium/Saccades conventions. Replace the following:

- Package/PACKAGE/package: with the name of your library
- Template: with the name of your class(es)
